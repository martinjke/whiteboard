using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace whiteboard.Hubs
{
    public class DrawingHub : Hub
    {
        public class DrawingData
        {
            public double X0 { get; set; }
            public double Y0 { get; set; }
            public double X1 { get; set; }
            public double Y1 { get; set; }
            public string Color { get; set; }
        }
        public async Task SendDrawing(DrawingData data)
        {
            await Clients.All.SendAsync("Drawing", data);
        }
    }
}